# Four-tap Grampler

A four-tap granular sampler written in Pure Data controlled with the monome arc and Korg nanoKontrol2. See [this video](https://vimeo.com/694369730/f493fbb41b) for a demo.

## Requirements

I wrote this to demo turns and using my own Pure Data libraries, so you'll need them all installed and available.

- [turns](https://gitlab.com/daveriedstra/turns) is software for controlling the monome arc
- [grains~](https://gitlab.com/daveriedstra/grains) is an abstraction for granular synthesis in Pure Data
- [dried-utils](https://gitlab.com/daveriedstra/dried-utils) is a set of utility abstractions I use in Pure Data. **Important:** this patch needs them to be available as `dried`, so either clone them into a folder with that name or symlink one (`ln -s dried-utils dried`).

## Usage

Plug in the arc and the nanoKontrol. Start up `main.pd` in Pure Data. Start up turns. Make sure the nanoKontrol is sending MIDI to both the arc and PureData (the UI in pd should reflect tweaks to the first four channel strips).

There are four granular taps reading from the same buffer. Record into the buffer with the record button on the nanoKontrol (toggle on / off) or load a sample into it (you'll have to dig into the subpatches a bit).

Each tap is controlled by one of the channel strips and a turns page. The remaining four channel strips and the rest of the buttons on the nanoKontrol are not assigned.

In general

- the slider is the tap's volume
- the knob is the tap's panning control
- S - starts granular playback
- M - pauses granular playback
- R - flips to this tap's turns page
- encoder 0 - quad-lfo: the waves are combined and modulate the `position range` (how far from the playhead a grain may randomly be drawn)
- encoder 1 - level: the relative level of that range as a fraction of the whole sample (fully counter-clockwise means no randomness, fully clockwise means any position in the buffer can be selected)
- encoder 2 - level or lfo: playhead position. The first and last taps are manually scrubbed while the middle two use a phasor (`saw-rising`)
- encoder 3 - value: the rate at which grains are fired and their relative length (higher means faster, but if you go beyond the maximum polyphony, grains~ will start dropping grains)

The fourth tap is configured to give shorter blippy sounds and is configured a little different than the other three:

- the knob controls grain size
- encoder 0 still controls deviation from playhead position but additionally grains are fired whenever its LFOs intersect
- encoder 3 - dual-lfo: the waves are combined and modulate the grain's playback rate (ie, pitch). This is quantized to multiples of 0.5 from 1 to 4.
